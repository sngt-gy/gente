package com.example.gente;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by sangeight on 08/04/2014.
 */
public class GenteListAdapter extends CursorAdapter {
    private LayoutInflater inflater;

    public GenteListAdapter(Context context, Cursor c) {
        super(context, c);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return inflater.inflate(R.layout.gente_listview, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView txtName = (TextView) view.findViewById(R.id.listDisplayName);
        TextView txtEmail = (TextView) view.findViewById(R.id.listDisplayEmail);
        TextView txtTel = (TextView) view.findViewById(R.id.listDisplayTel);

        Person p = new Person(cursor);

        String name = p.getFullName();
        String email = p.getEmail();
        String tel = p.getTel() + "";

        txtName.setText(name);
        txtEmail.setText(email);
        txtTel.setText(tel);
    }
}
