package com.example.gente;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

//TODO: Ideas for features: Search, Alphabet Index
//TODO: Select Photo
//TODO: Delete Contact
//TODO: Update Contact
//TODO: Groups
//TODO: Design

public class HomeActivity extends ListActivity {
    private static final String TAG = "GENTE: Home - ";
    private GenteListAdapter listAdapter;
    private Button navAddNew;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        navAddNew = (Button) findViewById(R.id.navAddNewContact);

        setupListeners();
        setupListView();

    }

    private void setupListeners() {
        navAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HomeActivity.this, AcitivtyNewContact.class);
                startActivity(i);
                finish(); // Make sure we destroy the activity
            }
        });
    }
    private void setupListView() {
        //TODO: Cursor Adapter
        Uri uri = DBContract.PersonTable.CONTENT_URI;
        String[] projection = DBContract.PersonTable.KEY_ALL;

        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        listAdapter = new GenteListAdapter(HomeActivity.this, cursor);
        setListAdapter(listAdapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Log.d(TAG, "Request item details = " + id);

        Uri uri = Uri.parse(DBContract.PersonTable.CONTENT_URI + "/" + id);

        Intent i = new Intent(this, AcitivityPersonDetails.class);
        i.putExtra(DBContract.PersonTable.CONTENT_ITEM_TYPE, uri);

        startActivity(i);
        finish();
    }



}
