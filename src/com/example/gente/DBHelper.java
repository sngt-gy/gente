package com.example.gente;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by sangeight on 07/04/2014.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static final String TAG = "GENTE: DBHelper - ";

    public DBHelper(Context context) {
        super(context, DBContract.DB_NAME, null, DBContract.DB_VER);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBContract.PersonTable.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
        db.execSQL(DBContract.PersonTable.DROP_TABLE);
        onCreate(db);
    }
}
