package com.example.gente;

import android.content.ContentResolver;
import android.content.UriMatcher;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by sangeight on 08/04/2014.
 */
public final class DBContract {
    // Avoid creating any instance of this class
    public DBContract(){}

    public static final int DB_VER = 1;
    public static final String DB_NAME = "gente.db";
    public static final String AUTHORITY = "com.example.gente";

    public static final int PERSON_ALL = 1;
    public static final int PERSON_ID = 2;

    public static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    public static class PersonTable implements BaseColumns {
        // Avoid creating any instance of this class
        private PersonTable(){}

        public static final String TABLE_NAME = "Person";
        public static final String KEY_FNAME = "FName";
        public static final String KEY_LNAME = "LName";
        public static final String KEY_TEL = "Telephone";
        public static final String KEY_EMAIL = "Email";
        public static final String KEY_IMG_URI = "Image";

        public static final String[] KEY_ALL
                = { _ID, KEY_FNAME, KEY_LNAME, KEY_TEL, KEY_EMAIL, KEY_IMG_URI};

        public static final Uri CONTENT_URI
                = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);

        public static final String CONTENT_TYPE
                = ContentResolver.CURSOR_DIR_BASE_TYPE + "/com.example.gente.person";

        public static final String CONTENT_ITEM_TYPE
                = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/com.example.gente.person";

        public static final String CREATE_TABLE
                = "CREATE TABLE "
                + TABLE_NAME
                + "("
                        + _ID + " INTEGER PRIMARY KEY, "
                        + KEY_FNAME + " TEXT, "
                        + KEY_LNAME + " TEXT, "
                        + KEY_TEL + " TEXT, "
                        + KEY_EMAIL + " TEXT, "
                        + KEY_IMG_URI + " TEXT"
                + ");";

        public static final String DROP_TABLE =
                "DROP TABLE IF EXISTS " + TABLE_NAME;


    }

    static {
        uriMatcher.addURI(AUTHORITY, PersonTable.TABLE_NAME, PERSON_ALL);
        uriMatcher.addURI(AUTHORITY, PersonTable.TABLE_NAME +"/#", PERSON_ID);
    }





}
