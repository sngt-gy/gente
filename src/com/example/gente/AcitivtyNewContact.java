package com.example.gente;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.*;

/**
 * Created by sangeight on 07/04/2014.
 */
public class AcitivtyNewContact extends Activity {
    private static final String TAG = "GENTE:Activity/New Contact- ";

    private final int IMAGE_PICKER = 85;
    private final String KEY_SELECTED_IMG_URI = "selected-image-uri";
    private final String KEY_SELECTED_IMG_BMP = "selected-image-BMP";
    private Uri imgUri;
    private Bitmap bmpImage;
    private ImageView imgPersonImage;
    private EditText txtFName, txtLName, txtTel, txtEmail;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.new_contact);

        txtFName = (EditText) findViewById(R.id.contactFirstName);
        txtLName = (EditText) findViewById(R.id.contactLastName);
        txtTel = (EditText) findViewById(R.id.contactTel);
        txtEmail = (EditText) findViewById(R.id.contactEmail);
        imgPersonImage = (ImageView) findViewById(R.id.personImage);
        btnSubmit = (Button) findViewById(R.id.btnAddNewContact);

        if(savedInstanceState != null ) {
            imgUri = Uri.parse(savedInstanceState.getString(KEY_SELECTED_IMG_URI));
            bmpImage = savedInstanceState.getParcelable(KEY_SELECTED_IMG_BMP);
            imgPersonImage.setImageBitmap(bmpImage);
        }
        setUpListeners();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(imgUri != null ) {
            outState.putString(KEY_SELECTED_IMG_URI, imgUri.toString());
            outState.putParcelable(KEY_SELECTED_IMG_BMP, bmpImage);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //TODO: Handle Google Drive Images

        if(resultCode == RESULT_OK) {
            if(requestCode == IMAGE_PICKER) {
                imgUri = data.getData();
//                String  imgPath = getPath(imgUri);
//                Log.d(TAG, "Path to image" + imgUri + ", " + imgUri);
                try{
                    Bitmap originalBmp = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                    bmpImage = Utils.ImageWorker.resizeBitmap(
                            originalBmp, Utils.ImageWorker.DEFAULT_IMAGE_WIDTH, Utils.ImageWorker.DEFAULT_IMAGE_HEIGHT);

                    originalBmp.recycle();

                    imgPersonImage.setImageBitmap(bmpImage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setUpListeners() {
        imgPersonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Image clicked");

                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, "Choose Contact Photo"), IMAGE_PICKER);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fname = txtFName.getText().toString();
                String lname = txtLName.getText().toString();
                String email = txtEmail.getText().toString();
                String img = imgUri.toString();
                long tel = 0;

                if(txtTel.getText().toString().length() > 0 )
                    tel = Integer.parseInt(txtTel.getText().toString());

                ContentValues values = new ContentValues();

                values.put(DBContract.PersonTable.KEY_FNAME, fname);
                values.put(DBContract.PersonTable.KEY_LNAME, lname);
                values.put(DBContract.PersonTable.KEY_TEL, tel);
                values.put(DBContract.PersonTable.KEY_EMAIL, email);
                values.put(DBContract.PersonTable.KEY_IMG_URI, img);

                getContentResolver().insert(DBContract.PersonTable.CONTENT_URI, values);

                //Go back to Home Activity
                Intent i = new Intent(AcitivtyNewContact.this, HomeActivity.class);
                startActivity(i);
                finish(); // Make sure we destroy the activity

            }
        });
    }
}
