package com.example.gente;

import android.database.Cursor;
import android.net.Uri;

/**
 * Created by sangeight on 07/04/2014.
 */
public class Person {
    private int id, tel;
    private String firstName, lastName, email, imgUri;

    public Person(String firstName, String lastName, int tel, String email, String imgUri) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.tel = tel;
        this.imgUri = imgUri;
    }

    public Person(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(DBContract.PersonTable._ID));
        this.firstName = cursor.getString(cursor.getColumnIndex(DBContract.PersonTable.KEY_FNAME));
        this.lastName = cursor.getString(cursor.getColumnIndex(DBContract.PersonTable.KEY_LNAME));
        this.tel = cursor.getInt(cursor.getColumnIndex(DBContract.PersonTable.KEY_TEL));
        this.email = cursor.getString(cursor.getColumnIndex(DBContract.PersonTable.KEY_EMAIL));
        this.imgUri = cursor.getString(cursor.getColumnIndex(DBContract.PersonTable.KEY_IMG_URI));
    }

    public int getTel() {
        return tel;
    }

    public void setTel(int tel) {
        this.tel = tel;
    }

    public Person(int id, String firstName, String lastName, int tel, String email, String imgUri) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.tel = tel;
        this.email = email;
        this.imgUri = imgUri;
    }

    public int getId() {
        return id;
    }

    public String getImgUri() {
        return imgUri;
    }

    public void setImgUri(String imgUri) {
        this.imgUri = imgUri;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return firstName +" "+ lastName;
    }

    public String toString() {
        String output;
        output  = "----";
        output += "ID: "+ id;
        output += "Name: " + firstName +" "+ lastName;
        output += "Telephone: " + tel;
        output += "Photo Uri: " + imgUri;

        return output;
    }

}