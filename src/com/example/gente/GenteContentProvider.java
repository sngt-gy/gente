package com.example.gente;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import java.sql.SQLException;

/**
 * Created by sangeight on 08/04/2014.
 */
public class GenteContentProvider extends ContentProvider {
    private static final String TAG = "GENTE: Content Provider - ";
    private static final String AUTHORITY = "com.example.provider.Gente";
    private static Uri PRO_URL = Uri.parse("content://"+ AUTHORITY);

    private DBHelper dbHelper;

    @Override
    public boolean onCreate() {
        dbHelper = new DBHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projections, String selection, String[] selectionOpts, String sortType) {
        SQLiteQueryBuilder qBuilder = new SQLiteQueryBuilder();

        switch (DBContract.uriMatcher.match(uri)) {
            case DBContract.PERSON_ALL:
                qBuilder.setTables(DBContract.PersonTable.TABLE_NAME);
                break;
            case DBContract.PERSON_ID:
                qBuilder.setTables(DBContract.PersonTable.TABLE_NAME);
                qBuilder.appendWhere(
                    DBContract.PersonTable._ID
                    + "=" + uri.getLastPathSegment()
                );
                break;
            default:
                throw new IllegalArgumentException("Invalid Uri: " + uri);
        }

        Cursor cursor = qBuilder.query(
                dbHelper.getReadableDatabase(),
                projections, selection, selectionOpts, null, null, sortType
        );

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    // TODO: This method seems redundant. Remove it 
    @Override
    public String getType(Uri uri) {
        switch (DBContract.uriMatcher.match(uri)) {
            case DBContract.PERSON_ALL:
                return DBContract.PersonTable.CONTENT_TYPE;
            case DBContract.PERSON_ID:
                return DBContract.PersonTable.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Invalid Uri: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String tableName = null;

        switch(DBContract.uriMatcher.match(uri)) {
            case DBContract.PERSON_ALL:
            case DBContract.PERSON_ID:
                tableName = DBContract.PersonTable.TABLE_NAME;
                break;
        }

        long id = db.insert(tableName, null, contentValues);
        Log.d(TAG, contentValues.toString());
        // only if the record was added
        if(id > 0) {
            Uri u = ContentUris.withAppendedId(DBContract.PersonTable.CONTENT_URI, id);
            getContext().getContentResolver().notifyChange(u, null);
            return u;
        } else {
            throw new android.database.SQLException("Couldn't insert new record. Uri: " + uri);
        }
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionOpts) {
        int rows = 0;
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        switch(DBContract.uriMatcher.match(uri)) {
            case DBContract.PERSON_ALL:
                rows = db.update(
                   DBContract.PersonTable.TABLE_NAME,
                   contentValues, selection, selectionOpts
                );
                break;
            case DBContract.PERSON_ID:
                String id = uri.getLastPathSegment();
                String select = DBContract.PersonTable._ID +"="+ id;

                if(selection !=null && !selection.isEmpty()) {
                    select += " and " + selection;
                }
                rows = db.update(
                    DBContract.PersonTable.TABLE_NAME,
                    contentValues, select, selectionOpts
                );

                break;
            default:
                throw new IllegalArgumentException("Invalid Uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rows;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionOpts) {
        int rows = 0;
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        switch(DBContract.uriMatcher.match(uri)) {
            case DBContract.PERSON_ALL:
                rows = db.delete(
                    DBContract.PersonTable.TABLE_NAME,
                    selection, selectionOpts
                );
                break;
            case DBContract.PERSON_ID:
                String id = uri.getLastPathSegment();
                String select = DBContract.PersonTable._ID +"="+ id;

                if(selection !=null && !selection.isEmpty()){
                    select += " and " + selection;
                }

                rows = db.delete(
                    DBContract.PersonTable.TABLE_NAME,
                    select, selectionOpts
                );
                break;
            default:
                throw new IllegalArgumentException("Invalid Uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rows;
    }
}
