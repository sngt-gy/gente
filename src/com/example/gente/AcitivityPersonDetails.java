package com.example.gente;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by sangeight on 08/04/2014.
 */
public class AcitivityPersonDetails extends Activity {
    private static final String TAG = "GENTE: Details - ";
    private TextView txtName, txtEmail, txtTel;
    private Button btnEdit, btnDelete;
    private Bitmap bmpImage;
    private ImageView imgPerson;
    private Person person;
    private Uri personUri, imgUri;

    //TODO: Cache image

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_details);

        imgPerson = (ImageView) findViewById(R.id.detailsImg);
        txtName = (TextView) findViewById(R.id.detailsName);
        txtEmail = (TextView) findViewById(R.id.detailsEmail);
        txtTel = (TextView) findViewById(R.id.detailsTel);
        btnEdit = (Button) findViewById(R.id.btnEditContact);
        btnDelete = (Button) findViewById(R.id.btnDeleteContact);

        Bundle extras = getIntent().getExtras();

        Bundle bundle = extras;

        personUri = bundle.getParcelable(DBContract.PersonTable.CONTENT_ITEM_TYPE);

        setupListeners();
        displayDetails();

    }

    private void setupListeners() {
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getContentResolver().delete(personUri, null, null);
                Log.d(TAG, "Person deleted " + personUri.getLastPathSegment());

                Intent i = new Intent(AcitivityPersonDetails.this, HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    private void displayDetails() {
        String[] projection = DBContract.PersonTable.KEY_ALL;
        Cursor cursor = getContentResolver().query(personUri, projection,null,null,null);

        if(cursor != null) {
            cursor.moveToFirst();
            person = new Person(cursor);
            txtName.setText(person.getFullName());
            txtEmail.setText(person.getEmail());
            txtTel.setText(person.getTel()+"");
            imgUri = Uri.parse(person.getImgUri());

            if(imgUri != null) {
                Log.d(TAG, "Awesome we got photo" + imgUri.toString());
                try{
                    Bitmap originalBmp = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                    bmpImage = Utils.ImageWorker.resizeBitmap(
                            originalBmp, Utils.ImageWorker.DEFAULT_IMAGE_WIDTH, Utils.ImageWorker.DEFAULT_IMAGE_HEIGHT);
                    originalBmp.recycle();

                    imgPerson.setImageBitmap(bmpImage);
                    Log.d(TAG, "WE GOT HERE ]\n] " + imgPerson.toString() + "\n" + originalBmp.toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


    }

}
