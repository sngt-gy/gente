package com.example.gente;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;

import java.io.FileNotFoundException;

/**
 * Created by sangeight on 07/04/2014.
 */
public final class Utils {


    /* Image Manipulation */
    public final static class ImageWorker {
        public ImageWorker(){}
        public static final int DEFAULT_IMAGE_WIDTH = 200;
        public static final int DEFAULT_IMAGE_HEIGHT = 200;


        public static Bitmap resizeBitmap(Bitmap bmp, int targetHeight, int targetWidth) {

            int bmpWidth = bmp.getWidth(),
                bmpHeight = bmp.getHeight();

            float scaleX = ((float) targetWidth) / bmpWidth,
                scaleY = ((float) targetHeight) / bmpHeight;

            Matrix matrix = new Matrix();
            matrix.postScale(scaleX,scaleY);

            Bitmap outputBmp = Bitmap.createBitmap(bmp, 0, 0, bmpWidth, bmpHeight, matrix, false);
            return outputBmp;
        }
    }

}
